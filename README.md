My try of making games witch portals.
I make it in difrent way than most of others
for exmple using camera lens for moving
Example code:
```
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;


namespace Portal
{
    public class MovingLensThanFerment : MonoBehaviour
    {
        [SerializeField] private GameObject _portal;

        private Camera _cameraPortal,_mainCamera;
        private GameObject _planePortal,_pseudoMainCamera;
        private float _frustumHeight, _distance, _frustumWidth,_lensWidth,_lensHeight;

        // Start is called before the first frame update

        private void Awake()
        {
            _portal = this.gameObject;
            _cameraPortal = _portal.transform.GetComponentInChildren<Camera>();
            _planePortal= _portal.transform.GetChild(0).gameObject;
            _pseudoMainCamera= _portal.transform.GetComponentInChildren<PseudoMainCamera>().gameObject;
        }
        // Update is called once per frame
        void FixedUpdate()
        {
            _distance = Vector3.Distance(_cameraPortal.transform.position, _planePortal.transform.position);
            _frustumHeight = 2.0f * _distance * Mathf.Tan(_cameraPortal.fieldOfView * 0.5f * Mathf.Deg2Rad);
            _frustumWidth = _frustumHeight * _cameraPortal.aspect;
            _lensWidth = (_planePortal.transform.position.x - _pseudoMainCamera.transform.position.x) / _frustumWidth;
            _lensHeight = (_planePortal.transform.position.y - _pseudoMainCamera.transform.position.y) / _frustumHeight;
            _cameraPortal.lensShift = new Vector2(_lensWidth, _lensHeight);
        }
    }
}
```
