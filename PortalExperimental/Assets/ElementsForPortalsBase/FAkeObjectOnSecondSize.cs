﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Portal
{
    public class FAkeObjectOnSecondSize : MonoBehaviour
    {
        private List<GameObject> _objectInPortals = new List<GameObject>();
        private GameObject _portalTrue, _portalFake;
        private void Awake()
        {
            _portalTrue = this.transform.parent.gameObject;
            if (_portalTrue.name == "Portal")
            {
                _portalFake = PortalSingleton.Singleton.Portal2;
            }
            else
            {
                _portalFake = PortalSingleton.Singleton.Portal1;
            }
        }
        private void OnTriggerEnter(Collider other)
        {
            if(!_objectInPortals.Contains(other.gameObject))
            {
                _objectInPortals.Add(other.gameObject);
            }
        }
        private void OnTriggerExit(Collider other)
        {
            if (_objectInPortals.Contains(other.gameObject))
            {
                _objectInPortals.Remove(other.gameObject);
            }
        }
        private void FixedUpdate()
        {
            for (int i = 0; i < _objectInPortals.Count; i++)
            {
                ControlFake(_objectInPortals[i]);
            }
        }
        private void ControlFake(GameObject _objectInPortal)
        {
          
        }
    }
}