﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Portal
{
    public class PortalSingleton : MonoBehaviour
    {
        // Start is called before the first frame update
        private static PortalSingleton _singleton;
        public GameObject mainCamera, Portal1, Portal2;
        public static PortalSingleton Singleton
        {
            get
            {
                return _singleton;
            }
            set
            {
                _singleton = value;
            }
        }
        private void Awake()
        {
            _singleton = this;
        }
        

    }
}