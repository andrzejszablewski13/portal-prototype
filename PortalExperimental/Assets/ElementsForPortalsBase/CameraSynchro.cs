﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Portal
{
    public class CameraSynchro : MonoBehaviour
    {
        // Start is called before the first frame update
        private Camera _cameraPortal1, _cameraPortal2;
        private GameObject _pseudoMainCamera1, _pseudoMainCamera2;
        private float _distanceZ, _distanceX, _distanceY;
        private float _correctPrecision = 0.1f;
        void Start()
        {
            _cameraPortal1 = PortalSingleton.Singleton.Portal1.GetComponentInChildren<Camera>();
            _cameraPortal2 = PortalSingleton.Singleton.Portal2.GetComponentInChildren<Camera>();
            _pseudoMainCamera1 = PortalSingleton.Singleton.Portal1.GetComponentInChildren<PseudoMainCamera>().gameObject;
            _pseudoMainCamera2 = PortalSingleton.Singleton.Portal2.GetComponentInChildren<PseudoMainCamera>().gameObject;
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            SetCameraPosition(_cameraPortal2, PortalSingleton.Singleton.Portal1, _pseudoMainCamera2);
            SetCameraPosition(_cameraPortal1, PortalSingleton.Singleton.Portal2, _pseudoMainCamera1);
        }
        private void SetCameraPosition(Camera _cameraToSet,GameObject _portal,GameObject _pseudoMainCamera)
        {
            _distanceX = PortalSingleton.Singleton.mainCamera.transform.position.x-_portal.transform.position.x;
            _distanceY = PortalSingleton.Singleton.mainCamera.transform.position.y-_portal.transform.position.y;
            _distanceZ = PortalSingleton.Singleton.mainCamera.transform.position.z- _portal.transform.position.z;
            _cameraToSet.transform.localPosition = new Vector3(_cameraToSet.transform.localPosition.x, _cameraToSet.transform.localPosition.y, _distanceZ);
            _pseudoMainCamera.transform.localPosition = new Vector3(_distanceX, _distanceY, _pseudoMainCamera.transform.localPosition.z);
            _pseudoMainCamera.transform.localEulerAngles = PortalSingleton.Singleton.mainCamera.transform.eulerAngles;
            _cameraToSet.nearClipPlane =Mathf.Abs( _distanceZ)+ _correctPrecision;
        }
    }
}