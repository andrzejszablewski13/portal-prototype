﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace Portal
{
    public class TeleportPortals : MonoBehaviour
    {
        public Vector3 _directionToPortal;
        public float _precision = 0.01f, _rbfactor = 0f, _precisionWitchVelocity,_angle;
        public bool distance, direction;
        private List<GameObject> _listOfObjects=new List<GameObject>();
        // Start is called before the first frame update
        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent<Rigidbody>(out _))
            {
                _listOfObjects.Add(other.gameObject);
            }
            DecideToTeleport(other.gameObject);
        }
        private void OnTriggerExit(Collider other)
        {
            if (_listOfObjects.Contains(other.gameObject))
                _listOfObjects.Remove(other.gameObject);
        }
        // Update is called once per frame
        private void Update()
        {
            for (int i = 0; i < _listOfObjects.Count; i++)
            {
                DecideToTeleport(_listOfObjects[i]);
            }
        }
        private void DecideToTeleport(GameObject other)
        {
            _directionToPortal = this.gameObject.transform.position- this.GetComponentInChildren<Camera>().transform.position;
            _angle = Vector3.SignedAngle(new Vector3(0.1f,0.1f,0.1f),this.transform.InverseTransformPoint(other.transform.position),Vector3.up);
            if (_angle>0)
            {
                Debug.Log("test " +  this.gameObject.name + " " + _angle);
                Teleport(other);
                _angle = 0;
                if (_listOfObjects.Contains(other.gameObject))
                    _listOfObjects.Remove(other.gameObject);
            }
        }
        private void Teleport(GameObject _toTeleport)
        {
            if (Vector3.Distance(PortalSingleton.Singleton.Portal1.transform.position, _toTeleport.transform.position) < Vector3.Distance(PortalSingleton.Singleton.Portal2.transform.position, _toTeleport.transform.position))
            {
                RotateAfterTeleport(_toTeleport, PortalSingleton.Singleton.Portal1, PortalSingleton.Singleton.Portal2);
                _toTeleport.transform.position += PortalSingleton.Singleton.Portal2.transform.position - PortalSingleton.Singleton.Portal1.transform.position;
            }
            else
            {
                RotateAfterTeleport(_toTeleport, PortalSingleton.Singleton.Portal2, PortalSingleton.Singleton.Portal1);
                _toTeleport.transform.position += PortalSingleton.Singleton.Portal1.transform.position - PortalSingleton.Singleton.Portal2.transform.position;
            }
        }
        private Vector3 _difrenceInRotation;
        private void RotateAfterTeleport(GameObject _toTeleport,GameObject Portal1,GameObject Portal2)
        {
            _difrenceInRotation = Portal1.transform.eulerAngles - Portal2.transform.eulerAngles;
            _difrenceInRotation = new Vector3(_difrenceInRotation.x, _difrenceInRotation.y - 180, _difrenceInRotation.z);
            if (Vector3.Distance(Portal1.transform.position, _toTeleport.transform.position) < 
                Vector3.Distance(Portal2.transform.position, _toTeleport.transform.position))
            {
                _toTeleport.GetComponent<Rigidbody>().velocity = Quaternion.Euler(_difrenceInRotation.x, _difrenceInRotation.y, _difrenceInRotation.z)
                    * _toTeleport.GetComponent<Rigidbody>().velocity;
            }
        }

    }
}