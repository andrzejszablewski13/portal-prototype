﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicMovement : MonoBehaviour
{
    // Start is called before the first frame update
    private Rigidbody _rb;
    [SerializeField] private float _speed = 5,_jumpForce=10;
    private float _mouseX, _mouseY;
    public float sensitivity = 10f;
    public float maxYAngle = 80f;
    private Vector2 currentRotation;
    void Start()
    {
        _rb = this.gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        _rb.AddRelativeForce(new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"))* _speed, ForceMode.Impulse);
        if(Input.GetButton("Fire3"))
        {
            _rb.velocity = Vector3.zero;
        }
        if (Input.GetButton("Jump"))
        {
            _rb.AddForce(Vector3.up * _jumpForce, ForceMode.Impulse);
        }
        /*_mouseX = Input.GetAxis("Mouse X");
        _mouseY = Input.GetAxis("Mouse Y");
        this.GetComponentInChildren<Camera>().transform.Rotate(new Vector3(-_mouseY,_mouseX, 0));*/
        currentRotation.x += Input.GetAxis("Mouse X") * sensitivity;
        currentRotation.y -= Input.GetAxis("Mouse Y") * sensitivity;
        currentRotation.x = Mathf.Repeat(currentRotation.x, 360);
        currentRotation.y = Mathf.Clamp(currentRotation.y, -maxYAngle, maxYAngle);
        Camera.main.transform.rotation = Quaternion.Euler(currentRotation.y, currentRotation.x, 0);
        if (Input.GetMouseButtonDown(0))
            Cursor.lockState = CursorLockMode.Locked;
        if (Input.GetMouseButtonDown(1))
            Cursor.lockState = CursorLockMode.None;
        this.gameObject.transform.rotation = Quaternion.Euler(0, currentRotation.x, 0);
    }

}
